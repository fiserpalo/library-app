<?php

namespace App\Form;

use App\Entity\Autor;
use App\Entity\Book;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('id',NumberType::class,[
                'required' => false
            ])
            ->add('name', TextType::class)
            ->add('isbn', TextType::class)
            ->add('price', NumberType::class)
            ->add('autor', TextType::class)
            ->add('category', EntityType::class, [
                'attr' => ['class' => 'form-control'],
                'class' => Category::class,
                'choice_label' => 'Name',
                'multiple' => false,
                'required' => false,
            ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'csrf_protection' => false,
        ]);
    }
}
