<?php

namespace App\Controller;


use App\Entity\Autor;
use App\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ApiController extends AbstractController
{


    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ApiController constructor.
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api", name="api_library_index")
     * @return Response
     */
    public function index(): Response
    {
        $bookRepo = $this->entityManager->getRepository(Book::class);

        return new JsonResponse(
            $this->serializer->serialize(['books' => $bookRepo->findAll()], 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route("/api/books", name="api_books_json")
     * @return Response
     */
    public function getBooks(): Response
    {
        $bookRepo = $this->entityManager->getRepository(Book::class);

        return new JsonResponse(
            $this->serializer->serialize($bookRepo->findAll(), 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }


    /**
     * @Route("/api/search", name="ajax_search", methods={"GET"})
     */
    public function searchAction(Request $request): JsonResponse
    {

        $requestString = $request->get('q');


        $entities = $this->entityManager->getRepository(Autor::class)->findAutorsByString($requestString);

        if (!$entities) {
            $result['entities']['error'] = "Nastala chyba.";
        } else {
            $result['entities'] = $this->getRealEntities($entities);
        }


        return new JsonResponse(
            $this->serializer->serialize(['autors' => $result['entities']], 'json'),
            Response::HTTP_OK,
            [],
            true
        );

    }

    /**
     * @param $entities
     * @return array
     */
    public function getRealEntities($entities)
    {

        foreach ($entities as $entity) {
            $realEntities[$entity->getId()] = $entity->getName();
        }

        return $realEntities;
    }


}
