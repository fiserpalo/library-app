<?php

namespace App\Controller;

use App\Entity\Autor;
use App\Entity\Book;
use App\Form\BookType;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Nette\Utils\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LibraryController extends AbstractController
{


    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * LibraryController constructor.
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SerializerInterface $serializer, EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->redirectToRoute("library");
    }

    /**
     * @Route("/library", name="library")
     * @param Request $request
     * @return Response
     */
    public function library(Request $request): Response
    {
        $autorRepo = $this->entityManager->getRepository(Autor::class);
        $bookRepo = $this->entityManager->getRepository(Book::class);
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);


        if ($form->isSubmitted()) {
            $book = $form->getData();
            if (gettype($book->getAutor()) ==='string')
            {
                $autor = new Autor();
                $dbAutor = $autorRepo->findAutorsByString($book->getAutor());
                if($dbAutor[0])
                {
                    $autor = $dbAutor[0];
                } else{
                    $autor->setName($book->getAutor());

                }


                $this->entityManager->persist($autor);
                $book->setAutor($autor);

            }

            $this->entityManager->persist($book);
            $this->entityManager->flush();

            $this->addFlash('success', 'Super kniha bola pridana!');

        } else {
            $this->addFlash('error', 'Nevalidne data!');
        }




        return $this->render('library/index.html.twig', [
            'error' => $form->getErrors(),
            'books' => $bookRepo->findAll(),
            'autors' => $autorRepo->findAll(),
            'form' => $form->createView(),
        ]);
    }
}

